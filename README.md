# anki-harec

Collection de paquets Anki pour réviser et se préparer à l’examen pour l'obtention du certificat d'opérateur radioamateur organisé par l'[ANFR](https://www.anfr.fr/gerer/radioamateurs/nos-missions "ANFR").

Le contenu s'appuie sur les cours organisés au [Radio-Club F8KCF](https://f8kcf.net//formation/ "F8KCF") à la MJC d'Annemasse. Vous êtes invités à vous rapprocher du club pour préparer votre examen à la license radioamateurs.

## Description
Anki est un programme qui vous aidera à apprendre plus facilement, beaucoup plus efficacement qu’avec des méthodes traditionnelles.

Anki repose sur deux concepts simples : *« la révision active »* et le *« système de répétitions espacées »*.

La *« révision active »* consiste à essayer de se rappeler la réponse à une question posée, contrairement à la « révision passive » où l’on relit, regarde ou écoute la réponse, sans se demander si on la connaît réellement. Des recherches sur le sujet ont montré que la révision active formait des souvenirs beaucoup plus durables que ceux de la révision passive.

Le *« système de répétitions »* met en œuvre l'étalement des révisions dans le temps, nous parvenons ainsi à mieux les mémoriser que si nous les concentrons toutes en une seule séance.

## Anki en action

![Anki](screenshot.png "Anki")

## Installation

Anki est open source et fonctionne sur Windows, Mac OSX, Linux/FreeBSD et une majorité de smartphones.

Pour plus d'information, voir le [manuel de Anki](https://apps.ankiweb.net/docs/manual.fr.html "manuel")

## License

 <p xmlns:cc="http://creativecommons.org/ns#" >This work is marked with <a href="https://creativecommons.org/publicdomain/zero/1.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC0 1.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1" alt=""><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/zero.svg?ref=chooser-v1" alt=""></a></p> 
